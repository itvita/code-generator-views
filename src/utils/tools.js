/**
* @Author : liu.q [916000612@qq.com]
* @Date : 2020/8/1 7:17 下午
* @Description : 获取随机id
*/
export function widgetRandomId(length) {
  let str = ''
  for (var i = 1; i <= length; i++) {
    str += 'x'
  }
  return str.replace(/[xy]/g, function(c) {
    const r = Math.random() * 10 | 0
    const v = c === 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
}
/**
* @Author : liu.q [916000612@qq.com]
* @Date : 2020/8/1 7:17 下午
* @Description : 格式化日期
*/
export function formatDate(date, fmt) {
  try {
    if (!(date instanceof Date)) {
      date = new Date(date)
    }
    if (fmt === undefined || fmt === '') {
      fmt = 'yyyy-MM-dd hh:mm:ss'
    }
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    const o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    }
    for (const k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        const str = o[k] + ''
        fmt = fmt.replace(RegExp.$1, RegExp.$1.length === 1 ? str : padLeftZero(str))
      }
    }
    return fmt
  } catch (e) {
    console.log(e)
    return '-'
  }
}

function padLeftZero(str) {
  return ('00' + str).substr(str.length)
}

/**
* @time : 2021-01-11 09:32:33 星期一
* @author : liu.q [916000612@qq.com]
* @desc : 深拷贝
*/
export function deepClone(obj) {
  // 判断拷贝的要进行深拷贝的是数组还是对象，是数组的话进行数组拷贝，对象的话进行对象拷贝
  var objClone = Array.isArray(obj) ? [] : {}
  // 进行深拷贝的不能为空，并且是对象或者是
  if (obj && typeof obj === 'object') {
    for (var key in obj) {
      // eslint-disable-next-line no-prototype-builtins
      if (obj.hasOwnProperty(key)) {
        if (obj[key] && typeof obj[key] === 'object') {
          objClone[key] = deepClone(obj[key])
        } else {
          objClone[key] = obj[key]
        }
      }
    }
  }
  return objClone
}
