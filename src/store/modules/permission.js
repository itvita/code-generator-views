import default_router from '@/router/default_router'

const permission = {
  state: {
    routers: [],
    active: [0],
    leftActive: [0]
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.routers = routers
    },
    SET_ACTIVE: (state, active) => {
      state.active = active
    },
    SET_LEFTACTIVE: (state, leftActive) => {
      state.leftActive = leftActive
    }
  },
  actions: {
    GenerateRoutes({ commit }, data) {
      return new Promise(resolve => {
        commit('SET_ROUTERS', default_router)
        resolve(default_router)
      })
    },
    setActive({ commit }, active) {
      return new Promise(resolve => {
        commit('SET_ACTIVE', active)
        resolve(active)
      })
    },
    setLeftActive({ commit }, leftActive) {
      return new Promise(resolve => {
        commit('SET_LEFTACTIVE', leftActive)
        resolve(leftActive)
      })
    }
  }
}

export default permission
