import { request } from '@/api/service.js'

/* get -params  post -data */
/**
* @time : 2021-01-20 14:53:32 星期三
* @author : liu.q [916000612@qq.com]
* @desc : 测试connection
*/
export function testConnection(parameter) {
  return request({
    url: globalConf.baseURL + '/code/generator/testConnection',
    method: 'get',
    params: parameter
  })
}

/**
* @time : 2021-01-20 15:22:58 星期三
* @author : liu.q [916000612@qq.com]
* @desc : 获取数据库表
*/
export function getTableInfos(parameter) {
  return request({
    url: globalConf.baseURL + '/code/generator/getTableInfos',
    method: 'get',
    params: parameter
  })
}

/**
* @time : 2021-01-20 15:23:09 星期三
* @author : liu.q [916000612@qq.com]
* @desc : 获取数据库表信息
*/
export function getColumnInfos(parameter) {
  return request({
    url: globalConf.baseURL + '/code/generator/getColumnInfos',
    method: 'get',
    params: parameter
  })
}

/**
* @time : 2021-01-28 14:29:20 星期四
* @author : liu.q [916000612@qq.com]
* @desc : 服务端代码生成
*/
export function createLocal(parameter) {
  return request({
    url: globalConf.baseURL + '/code/generator/createLocal',
    method: 'post',
    data: parameter
  })
}

/**
* @time : 2021-01-28 14:29:29 星期四
* @author : liu.q [916000612@qq.com]
* @desc : 前端代码生成
*/
export function createLocalPage(parameter) {
  return request({
    url: globalConf.baseURL + '/code/generator/createLocalPage',
    method: 'post',
    data: parameter
  })
}
