import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import './permission'
import VueRouter from 'vue-router'
import '@/styles/index.css'
import '@/styles/icon.css'
import message from 'ant-design-vue/lib/message'
import modal from 'ant-design-vue/lib/modal'

import { index } from 'afd-lib'
Vue.use(index)

Vue.use(message)
Vue.use(modal)
Vue.prototype.$confirm = modal.confirm
Vue.prototype.$message = message
Vue.prototype.$info = modal.info
Vue.prototype.$success = modal.success
Vue.prototype.$error = modal.error
Vue.prototype.$warning = modal.warning

import { service } from '@/utils/service.js'
import store from './store/'

Vue.config.productionTip = false
Vue.use(VueRouter)
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(to) {
  return VueRouterPush.call(this, to).catch(err => err)
}
Vue.prototype.$axios = service

import { formatDate } from '@/utils/tools'
Vue.prototype.$formatDate = formatDate

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
