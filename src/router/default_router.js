import { HomeLayout } from '@/layouts'

/* 设计器*/
import designer from '@/pages/designer/index'
import code from '@/pages/code/index'
import table from '@/pages/table/index'

export default [
  {
    id: '100',
    meta: { title: '代码生成器' },
    name: 'code',
    component: HomeLayout,
    path: '/code',
    redirect: '/code/index',
    children: [
      {
        id: '100100',
        path: '/code/index',
        name: 'codeIndex',
        component: code,
        meta: { title: '代码生成器' }
      }
    ]
  },
  {
    id: '200',
    meta: { title: '表单设计器' },
    name: 'designer',
    component: HomeLayout,
    path: '/',
    redirect: '/designer/index',
    children: [
      {
        id: '200100',
        path: '/designer/index',
        name: 'designerIndex',
        component: designer,
        meta: { title: '表单设计器' }
      }
    ]
  },
  {
    id: '300',
    meta: { title: '搜索列表设计器' },
    name: 'table',
    component: HomeLayout,
    path: '/table',
    redirect: '/table/index',
    children: [
      {
        id: '300100',
        path: '/table/index',
        name: 'tableIndex',
        component: table,
        meta: { title: '搜索列表设计器' }
      }
    ]
  }
]
