
import { makeUpHtml } from './formUtil/html'
import { beautifierConf, buildScript, buildStyle } from './formUtil'
import { makeUpJs } from './formUtil/js'
import { makeUpCss } from './formUtil/css'
import beautifier from 'beautifier'

export function formTemplate(formDesign) {
  const data = {
    config: JSON.parse(JSON.stringify(formDesign.formConf)),
    list: JSON.parse(JSON.stringify(formDesign.widgetList))
  }
  let htmlCode = makeUpHtml(data)
  htmlCode = beautifier.html(htmlCode, beautifierConf.html)

  let jsCode = makeUpJs(data)
  jsCode = beautifier.js(jsCode, beautifierConf.js)

  let cssCode = makeUpCss(data)
  cssCode = beautifier.css(cssCode, beautifierConf.js)
  return `${htmlCode}\n${buildScript(jsCode)}\n${buildStyle(cssCode)}`
}
