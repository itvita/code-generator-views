
import { beautifierConf, buildScript, buildStyle } from './tableUtil'
import { makeUpHtml } from './tableUtil/html'
import { makeUpJs } from './tableUtil/js'
import { makeUpCss } from './tableUtil/css'
import beautifier from 'beautifier'

export function tableTemplate(tableDesign) {
  const { searchItems, tableOptions, toolsArray } = tableDesign
  let htmlCode = makeUpHtml(searchItems, tableOptions, toolsArray)
  htmlCode = beautifier.html(htmlCode, beautifierConf.html)

  let jsCode = makeUpJs(searchItems, tableOptions, toolsArray)
  jsCode = beautifier.js(jsCode, beautifierConf.js)

  let cssCode = makeUpCss(searchItems, tableOptions, toolsArray)
  cssCode = beautifier.js(cssCode, beautifierConf.js)

  return `${htmlCode}\n${buildScript(jsCode)}\n${buildStyle(cssCode)}`
}
