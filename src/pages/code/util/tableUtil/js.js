/**
 * 组装vue代码。【入口函数】
 * @param {Object} formConfig 整个表单配置
 */
export function makeUpJs(searchItems, tableOptions, toolsArray) {
  return buildJs(searchItems, tableOptions, toolsArray)
}
function buildJs(searchItems, tableOptions, toolsArray) {
  /* 判断是否需要引入删除api以及add */
  let dels = ''
  let delsMethod = ''
  let showAddMethod = ''
  let importAdd = ''
  let componentsAdd = ''
  toolsArray.forEach(v => {
    if (v.sites === 'dels') {
      dels = `import { dels } from '@/api/server/\${jsApiName}'`
      delsMethod = `
        dels () {
          const ids = this.$refs.myTable.selectedRowKeys
          if (ids.length <= 0) {
            this.$message.warning('请选择要删除的行！')
            return
          }
          this.$confirm({
            title: '确认删除？',
            content: '删除后将无法恢复！',
            onOk: () => {
              dels(ids).then(res => {
                if (res.code === 1) {
                  this.$message.success('删除成功')
                  this.$refs.myTable.selectedRowKeys = []
                  this.search()
                }
              })
            }
          })
        }
      `
    }
    if (v.sites === 'add') {
      showAddMethod = `showAdd () {
        this.$refs.edit.showModal()
      },`
      importAdd = `import edit from './edit'`
      componentsAdd = `edit`
    }
  })

  tableOptions.operate.items.forEach(oper => {
    if (oper.sites === 'edit') {
      if (showAddMethod === '') {
        showAddMethod = `showAdd () {
          this.$refs.edit.showModal()
        },`
        importAdd = `import edit from './edit'`
        componentsAdd = `edit`
      }
    }
    if (oper.sites === 'dels') {
      dels = `import { dels } from '@/api/server/\${jsApiName}'`
    }
  })
  return `
/**
 * @Author : \${author}
 * @Date : \${time}
 * @Description : 本页代码出自\${author}之手
 */
${dels}
${importAdd}
export default {
  components: { ${componentsAdd} },
  name: 'Index',
  data () {
    return {
      /* 搜索条件配置源 */
      items:${buildSearchItems(searchItems)}
    }
  },
  mounted () {
    /* 初始化表格 */
    this.$refs.myTable.init(
      {
        ${buildTableOptions(tableOptions)}
      },
      () => {
        this.search()
      }
    )
  },
  methods: {
    /* 搜索方法 */
    search (param) {
      this.$refs.myTable.search(param || {})
    },
    ${showAddMethod}
    ${delsMethod}
  }
}
  `
}
/**
* @time : 2021-01-11 17:37:06 星期一
* @author : liu.q [916000612@qq.com]
* @desc : 搜索条
*/
function buildSearchItems(searchItems) {
  let items = '['
  searchItems.forEach(item => {
    items += `
      {
        dataIndex: '${item.dataIndex}',
        label: '${item.label}',
        type: '${item.type}'
      },
    `
  })
  items += ']'
  return items
}
function buildTableOptions(tableOptions) {
  let url = tableOptions.url
  if (url === '') {
    url = `\${action}/getList`
  }
  return `
    url: globalConf.baseURL + '${url}',
    method: '${tableOptions.method}',
    changeSize: ${tableOptions.changeSize},
    showCheckBox: ${tableOptions.showCheckBox},
    showColumns: ${tableOptions.showColumns},
    showIndex: ${tableOptions.showIndex},
    showRefresh: ${tableOptions.showRefresh},
    columns:  ${buildTableColumns(tableOptions.columns)},
    operate: ${buildTableOperate(tableOptions.operate)},
  `
}
function buildTableColumns(params) {
  params = params || []
  let str = '['
  params.forEach(col => {
    str += `
    {
      align: '${col.align}',
      field: '${col.field}',
      hidden: ${col.hidden},
      sorter: ${col.sorter},
      title: '${col.title}',
      width: ${col.width}
    },
    `
  })
  str += ']'
  return str
}
function buildTableOperate(params) {
  params = params || {}
  return `
  {
    width:${params.width || 200},
    items:${buildTableOperateItems(params.items)}
  }
 `
}
function buildTableOperateItems(params) {
  if (params && params.length > 0) {
    let str = '['
    params.forEach(item => {
      str += `
          {
            color: '${item.color}',
            event: ${getEvent(item)},
            label: '${item.label}'
          },
        `
    })
    str += ']'
    return str
  } else {
    return '[]'
  }
}
function getEvent(item) {
  if (item.sites === 'edit') {
    return `
      row => {
        this.$refs.edit.showModal(row.id)
      }
      `
  } else if (item.sites === 'dels') {
    return `
      row => {
        this.$confirm({
          title: '确认删除？',
          content: '删除后将无法恢复！',
          onOk: () => {
            dels([row.id]).then(res => {
              if (res.code === 1) {
                this.$message.success('删除成功')
                this.$refs.myTable.selectedRowKeys = []
                this.search()
              }
            })
          }
        })
      }
      `
  } else {
    return `
        row=>{
          console.log(row)
        }
      `
  }
}
