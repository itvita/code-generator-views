/**
 * 组装vue代码。【入口函数】
 * @param {Object} formConfig 整个表单配置
 */
export function makeUpHtml(searchItems, tableOptions, toolsArray) {
  const searchTemplate = buildSearch(searchItems)
  const tableTemplate = buildTable(toolsArray)
  const edit = buildEdit(tableOptions, toolsArray)
  return buildTempalte(`${searchTemplate}${tableTemplate}`, edit)
}
function buildTempalte(str, edit) {
  return `
  <template>
    <a-card :bordered="false" size="small">
      ${str}
      ${edit}
    </a-card>
  </template>
  `
}
function buildEdit(tableOptions, toolsArray) {
  let _b = false
  toolsArray.forEach(tools => {
    if (tools.sites === 'add') {
      _b = true
    }
  })
  tableOptions.operate.items.forEach(oper => {
    if (oper.sites === 'edit') {
      _b = true
    }
  })
  if (_b) {
    return `<edit ref="edit" @search="search"></edit>`
  } else {
    return ''
  }
}
/**
* @time : 2021-01-11 17:37:06 星期一
* @author : liu.q [916000612@qq.com]
* @desc : 搜索条
*/
function buildSearch(searchItems) {
  if (searchItems.length > 0) {
    return `
      <lz-search ref="searchBar" :items="items" @search="search"></lz-search>
    `
  }
  return ''
}
/**
* @time : 2021-01-11 17:36:50 星期一
* @author : liu.q [916000612@qq.com]
* @desc : table列表
*/
function buildTable(toolsArray) {
  return `
  <lz-table ref="myTable">
    <div slot="tools">
      ${buildTools(toolsArray)}
    </div>
  </lz-table>
  `
}
/**
* @time : 2021-01-12 09:07:36 星期二
* @author : liu.q [916000612@qq.com]
* @desc : 工具条按钮
*/
function buildTools(toolsArray) {
  let str = ``
  toolsArray.forEach(tools => {
    let clickStr = ''
    if (tools.sites === 'add') {
      clickStr = `@click="showAdd"`
    } else if (tools.sites === 'dels') {
      clickStr = ` @click="dels"`
    }
    str += `<a-button icon="${tools.icon}" ${clickStr} style="margin-right:10px" type="${tools.type}">${tools.label}</a-button>`
  })
  return str
}
