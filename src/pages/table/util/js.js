/**
 * 组装vue代码。【入口函数】
 * @param {Object} formConfig 整个表单配置
 */
export function makeUpJs(searchItems, tableOptions, toolsArray) {
  console.log(searchItems, tableOptions, toolsArray)
  return buildJs(searchItems, tableOptions)
}
function buildJs(searchItems, tableOptions) {
  return `
export default {
  components: { },
  name: 'Index',
  data () {
    return {
      items:${buildSearchItems(searchItems)}
    }
  },
  mounted () {
    this.$refs.myTable.init(
      {
        ${buildTableOptions(tableOptions)}
      },
      () => {
        this.search()
      }
    )
  },
  methods: {
    search (param) {
      this.$refs.myTable.search(param || {})
    }
  }
}
  `
}
/**
* @time : 2021-01-11 17:37:06 星期一
* @author : liu.q [916000612@qq.com]
* @desc : 搜索条
*/
function buildSearchItems(searchItems) {
  let items = '['
  searchItems.forEach(item => {
    items += `
      {
        dataIndex: '${item.dataIndex}',
        label: '${item.label}',
        type: '${item.type}'
      },
    `
  })
  items += ']'
  return items
}
function buildTableOptions(tableOptions) {
  return `
    url: '${tableOptions.url}',
    method: '${tableOptions.method}',
    changeSize: ${tableOptions.changeSize},
    showCheckBox: ${tableOptions.showCheckBox},
    showColumns: ${tableOptions.showColumns},
    showIndex: ${tableOptions.showIndex},
    showRefresh: ${tableOptions.showRefresh},
    columns:  ${buildTableColumns(tableOptions.columns)},
    operate: ${buildTableOperate(tableOptions.operate)},
  `
}
function buildTableColumns(params) {
  params = params || []
  let str = '['
  params.forEach(col => {
    str += `
    {
      align: '${col.align}',
      field: '${col.field}',
      hidden: ${col.hidden},
      sorter: ${col.sorter},
      title: '${col.title}',
      width: ${col.width}
    },
    `
  })
  str += ']'
  return str
}
function buildTableOperate(params) {
  params = params || {}
  return `
  {
    width:${params.width || 200},
    items:${buildTableOperateItems(params.items)}
  }
 `
}
function buildTableOperateItems(params) {
  if (params && params.length > 0) {
    let str = '['
    params.forEach(item => {
      str += `
          {
            color: '${item.color}',
            event: ${item.event},
            label: '${item.label}'
          },
        `
    })
    str += ']'
    return str
  } else {
    return '[]'
  }
}

