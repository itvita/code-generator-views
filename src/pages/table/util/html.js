/**
 * 组装vue代码。【入口函数】
 * @param {Object} formConfig 整个表单配置
 */
export function makeUpHtml(searchItems, tableOptions, toolsArray) {
  const searchTemplate = buildSearch(searchItems)
  const tableTemplate = buildTable(toolsArray)
  return buildTempalte(`${searchTemplate}${tableTemplate}`)
}
function buildTempalte(str) {
  return `
  <template>
    <a-card :bordered="false" size="small">
      ${str}
    </a-card>
  </template>
  `
}
/**
* @time : 2021-01-11 17:37:06 星期一
* @author : liu.q [916000612@qq.com]
* @desc : 搜索条
*/
function buildSearch(searchItems) {
  if (searchItems.length > 0) {
    return `
      <lz-search ref="searchBar" :items="items" @search="search"></lz-search>
    `
  }
  return ''
}
/**
* @time : 2021-01-11 17:36:50 星期一
* @author : liu.q [916000612@qq.com]
* @desc : table列表
*/
function buildTable(toolsArray) {
  return `
  <lz-table ref="myTable">
    <div slot="tools">
      ${buildTools(toolsArray)}
    </div>
  </lz-table>
  `
}
/**
* @time : 2021-01-12 09:07:36 星期二
* @author : liu.q [916000612@qq.com]
* @desc : 工具条按钮
*/
function buildTools(toolsArray) {
  let str = ``
  toolsArray.forEach(tools => {
    str += `<a-button icon="${tools.icon}" style="margin-right:10px" type="${tools.type}">${tools.label}</a-button>`
  })
  return str
}
