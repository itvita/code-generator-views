const path = require('path')
const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin')
const assetsCDN = {
  // webpack build externals
  externals: {
    vue: 'Vue',
    'vue-router': 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios',
    monaco: 'monaco',
    'ant-design-vue': 'ant-design-vue',
    beautifier: 'beautifier',
    moment: 'moment',
    'AMap': 'AMap',
    'window.jQuery': 'jquery',
    '$': 'jquery'
  },
  css: [
    'https://fastly.jsdelivr.net/npm/ant-design-vue@1.6.4/dist/antd.min.css'
  ],
  js: [
    'https://unpkg.zhimg.com/vue@2.6.11/dist/vue.min.js',
    'https://unpkg.zhimg.com/vue-router@3.1.3/dist/vue-router.min.js',
    'https://unpkg.zhimg.com/vuex@3.1.1/dist/vuex.min.js',
    'https://unpkg.zhimg.com/axios@0.19.0/dist/axios.min.js',
    'https://lf6-cdn-tos.bytecdntp.com/cdn/expire-1-M/js-beautify/1.10.2/beautifier.min.js',
    'https://fastly.jsdelivr.net/npm/moment@2.27.0/moment.min.js',
    'https://fastly.jsdelivr.net/npm/moment@2.27.0/locale/zh-cn.js',
    'https://fastly.jsdelivr.net/npm/ant-design-vue@1.6.4/dist/antd.min.js',
    'https://unpkg.zhimg.com/jquery@3.5.1/dist/jquery.min.js'
  ]
}

module.exports = {
  // publicPath: '/ant-form-designer',
  outputDir: 'dist', // 构建输出目录
  assetsDir: 'assets', // 静态资源目录 (js, css, img, fonts)
  lintOnSave: true, // 是否开启eslint保存检测，有效值：ture | false | 'error'
  runtimeCompiler: true, // 运行时版本是否需要编译
  transpileDependencies: [], // 默认babel-loader忽略mode_modules，这里可增加例外的依赖包名
  productionSourceMap: false, // 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: '表单设计器',
      cdn: assetsCDN
    },
    preview: {
      // page 的入口
      entry: 'src/previewMain.js',
      // 模板来源
      template: 'public/preview.html',
      // 在 dist/index.html 的输出
      filename: 'preview.html',
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: '预览',
      // 在这个页面中包含的块，默认情况下会包含
      // 提取出来的通用 chunk 和 vendor chunk。
      chunks: ['chunk-vendors', 'chunk-common', 'preview'],
      cdn: assetsCDN
    }
  },
  css: { // 配置高于chainWebpack中关于css loader的配置
    // modules: true, // 是否开启支持‘foo.module.css’样式
    // extract: true, // 是否使用css分离插件 ExtractTextPlugin，采用独立样式文件载入，不采用<style>方式内联至html文件中
    // requireModuleExtension: false,
    sourceMap: false, // 是否在构建样式地图，false将提高构建速度
    loaderOptions: {}
  },
  parallel: require('os').cpus().length > 1, // 构建时开启多进程处理babel编译
  devServer: {
    open: true,
    port: 8080
  },
  configureWebpack: {
    externals: assetsCDN.externals,
    resolve: {
      alias: {
        '@': path.resolve(__dirname, './src')
      }
    },
    plugins: [
      new MonacoWebpackPlugin()
    ]
  }
}
